var gulp = require('gulp'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  plumber = require('gulp-plumber'),
  cleanCSS = require('gulp-clean-css'),
  uglify = require('gulp-uglify'),
  pug = require('gulp-pug'),
  autoprefixer = require('gulp-autoprefixer'),
  babel = require('gulp-babel');

gulp.task('html', function(){
  return gulp.src('src/pug/**/*.pug')
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(pug())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('dev'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('css', function() {
  return gulp.src('src/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: [ 'last 2 versions' ],
      cascade: false
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('dev/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('js', function() {
  return gulp.src('src/js/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(babel({
      presets: [ 'env' ]
    }))
    .pipe(uglify())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('dev/js'))
    .pipe(browserSync.reload({
      stream: true
    }))
})

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: './dev'
    }
  })
})


gulp.task('watch', [ 'js', 'css', 'html', 'browser-sync' ], function() {
  gulp.watch('src/pug/**/*.pug', [ 'html' ])
  gulp.watch('src/markdown/**/*.md', [ 'html' ])
  gulp.watch('src/sass/**/*.scss', [ 'css' ])
  gulp.watch('src/js/**/*.js', [ 'js' ])
})

// task default
gulp.task('default', [])

// build

gulp.task('js_', function() {
  return gulp.src('src/js/**/*.js')
    .pipe(babel({
      presets: [ 'env' ]
    }))
    .pipe(uglify())
    .pipe(gulp.dest('dev/js'))
})

gulp.task('css_', function() {
  return gulp.src('src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: [ 'last 2 versions' ],
      cascade: false
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dev/css'))
});

gulp.task('html_', function(){
  return gulp.src('src/pug/**/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('dev'))
});

gulp.task('build', [ 'js_', 'html_', 'css_'], function() {
  gulp.src('dev/index.html')
    .pipe(gulp.dest('build'));
  gulp.src('dev/css/index.css')
    .pipe(gulp.dest('build/css'));
  gulp.src('dev/js/**/*.js')
    .pipe(gulp.dest('build/js'));
    console.log('fin de la tâche build\n')
})

// watch md file for redaction 
gulp.task('markdown', ['browser-sync'], function() {
  gulp.watch('src/markdown/**/*.md', [ 'html' ])
})

/** link:
  * autoprefixer: https://www.npmjs.com/package/gulp-autoprefixer
  * sourcemaps: https://www.npmjs.com/package/gulp-sourcemaps
  * plumber: https://www.npmjs.com/package/gulp-plumber
  * uglify: https://www.npmjs.com/package/gulp-uglify
  * pug: https://www.npmjs.com/package/gulp-pug
  * babel: https://www.npmjs.com/package/gulp-babel
  * clean-css: https://www.npmjs.com/package/gulp-clean-css
  * sass: https://github.com/dlmanning/gulp-sass
  * browserSync: https://browsersync.io/docs/gulp
  */

gulp.task('default', () => {
  console.log('gulp build ou npm run build pour build le projet')
  console.log('gulp watch pour le developpement avec browser-sync')
  console.log('gulp markdown pour la rédaction des articles avec le serveur browser-sync')
})